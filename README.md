<h1 align="center">No BS Search</h1>
<h2 align="center">A spam filter for your search results.</h2>

<p>There are already search engines that doesn't track you by themselves, but this extensions extends that respect to include the results they suggest aswell. A search engine extension that helps you avoid search results of sites that track you and contain unneccesary bloat such as complicated client scripts, popup windows and annoying ads.</p>

<h3>Functionality</h3>
<p>To date, only the html.duckduckgo-version has been developed. In the future there will be support for additional web-engines, and there will be three levers that let you customize your search spam filter:
<br>
<b>1) ANTITRACK.</b> Elements such as e.g. third party cookies, tracking pixels, and non-vital own domain cookies.<br>
<b>2) ANTIBLOAT.</b> Bloat in the form of massive client-side scripts, popups and other moving elements.<br>
<b>3) ANTIADS.</b> Annoying and invasive advertising.</p>

<h3>Target Search Engines</h3>
<p>Primarily privacy respecting alternatives such as e.g. duckduckgo, qwant and metager.</p>

<h3>Firefox Extension</h3>
<p>I am now utilizing duckduckgo tracking radar data to give search results a score based on amount of bs (i.e. tracking, bloat and ads).</p>

<h3>Contribute</h3>
<p>I welcome all contributions!</p>

<h3>License</h3>
<p>GPL v3.</p>
